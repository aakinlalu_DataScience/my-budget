# README #


This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To keep track of major expense variables.
+ The Variables are made of the following:
      * Amount: is the amount of money to keep track of.
      * Saving: is the 15% of amount. 
      * Title: is the 10% of amount.
      * Utility: is the 5% of amount.
      * Date: is the date the program is run.
      * The rest: is the amount -(Saving+Title+Utility).
- Version 1.0


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
    * inbuilt python modules
* Database configuration
    * First run will create json file for further storage.
* How to run tests
* Deployment instructions:
     * It is run at terminal
     * For example; To keep record of $500
     >  $python saving.py 500 '17/06/2015'

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact