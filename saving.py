# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 15:22:52 2015

@author: adebayo
"""
import sys
import json
import os


def Saving(Amount):
    '''
    10% of Amount is for God
    15% of Amount is for saving
    5% of Amount is utility
    '''
    GodMoney = Amount * 0.1
    Saving = Amount * 0.15
    Utility = Amount * 0.05
    The_rest = Amount - (GodMoney + Saving + Utility)
    return [GodMoney, Saving, Utility, The_rest]


def Budget(Amount):
    '''
    Store Amount, Saving, Utility, The_rest 
    into dictionary
    '''
    budget = {}
    Godmoney, saving, utility, the_rest = Saving(Amount)
    budget['Date'] = sys.argv[2]
    budget['Amount'] = Amount
    budget['Title'] = Godmoney
    budget['Saving'] = saving
    budget['Utility'] = utility
    budget['The_rest'] = the_rest
    return budget

def append_saving(Amount):
    '''
    Save the dictionary into a json file
    '''
    with open('../SpendingRecord', 'a') as feedjson:
        json.dump(Budget(Amount), feedjson)
        feedjson.write(os.linesep)
     


    
if __name__ == '__main__':
    Amount = sys.argv[1]
    breakdown = Saving(float(Amount))
    print('GodMoney:%8.2f, Saving:%8.2f, Utility: %8.2f, The_rest:%8.2f'% \
    (breakdown[0], breakdown[1], breakdown[2], breakdown[3]))   
    print(Budget(float(Amount)))
    append_saving(float(Amount))

    
# Saving = {
#            Amount:, GodMoney:, Saving:, }